## Javescript是什么样的语言
JavaScript是世界上最流行的脚本语言，因为你在电脑、手机、平板上浏览的所有的网页，以及无数基于HTML5的手机App，交互逻辑都是由JavaScript驱动的。

简单地说，JavaScript是一种运行在浏览器中的解释型的编程语言。

### 基本语法
1、执行顺序 JavaScript程序按照在HTML文件中出现的顺序逐行执行。 如果需要在整个HTML文件中执行（如函数、全局变量等），最好将其放在HTML文件的

...标记
2、字母大小写 JavaScript对字母大小写是敏感的（严格区分字母大小写）

3、空格与换行 JavaScript中的换行可代表“断句”，即换行表示一个语句已经结束。

4、每行结尾的分号 JavaScript并不要求必须以分号(;)作为语句的结束标记。

5、注释 JavaScript提供了两种注释符号，即"//"和"/.../"。 其中"//"用于单行注释。"/.../"用于多行注释。

### 数值类型
1、数值型：是JavaScript中最基本的数据类型，并不区分整型数值和浮点型数值
    a、十进制：
        在JAvaScript程序中，十进制的整数是一个由0~9的数字组成的数字序列

    b、十六进制：
        十六进制的数据是以0X或0x开头，其后跟随十六进制的数字序列。
        十六进制的数字可以是0~9的某个数字，也可以是a(A)~f(F)的某个字母。

    c、八进制：
        八进制数据以数字0开头，其后跟随一个数字序列，这个序列中的每个数字的取值范围都为0~7（包括0~7）。

    d、浮点型数据：
        （1）传统记数法。传统记数法是将一个浮点数分为整数部分、小数点和小数部分，如果整数部分为0，则可以省略整数部分。
        （2）科学记数法。使用科学记数法表示浮点型数据，即实数后跟随字母e或E，后面加上一个带正号或负号的整数指数，其中正号可以省略。、

    e、特殊值Infinity：
        特殊值----Infinity（无穷），如果一个数值超出了JAvaScript所能表示的最大值范围，那么就会输出Infinity；
        如果一个数值超出了JAvaScript所能表示的最小值范围，那么就会输出-INfinity。

    f、特殊值NaN
        特殊值----NaN（not a number的简写），即非数字。
        在进行数学运算时产生了未知的结果或错误，JAvaScript就会返回NaN，表示该数学运算的结果不是一个数字。
        例如，用0除以0的输出结果就是NaN。