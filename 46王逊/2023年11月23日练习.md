# 练习1

小明希望利用map()把字符串变成整数，他写的代码很简洁：
```js
var arr = ['1', '2', '3'];
var r;
r = arr.map(function (x) {
    return parseInt(x,10);
});

console.log(r);

```
# 练习2

练习 请把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa', 'Bart']。

```js
let arr=['adam', 'LISA', 'barT'];

function normalize(arr) {
    return arr.slice(0,1).toUpperCase()+arr.slice(1).toLowerCase();
}
let rest1=arr.map(normalize);
console.log(rest1);
// 测试:
if (normalize(['adam', 'LISA', 'barT']).toString() === ['Adam', 'Lisa', 'Bart'].toString()) {
    console.log('测试通过!');
}
else {
    console.log('测试失败!');
}


```
